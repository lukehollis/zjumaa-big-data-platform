FROM python:3.8

RUN apt-get update -y
RUN apt-get install -y --no-install-recommends postgresql-client
RUN apt-get install build-essential libssl-dev -y
ENV NODE_VERSION=14.15.4
RUN curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash
ENV NVM_DIR=/root/.nvm
RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm use v${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm alias default v${NODE_VERSION}
ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"
RUN node --version
RUN npm --version

WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# install dependencies
RUN pip install --upgrade pip
COPY big-data-platform/requirements.txt ./
RUN pip install -r requirements.txt
COPY big-data-platform/. .

# env vars
COPY big-data-platform/config/settings_local.example.py config/settings_local.py

# build statics
WORKDIR /usr/src/app
RUN python manage.py tailwind install
RUN python manage.py tailwind build
RUN python manage.py collectstatic --no-input

#EXPOSE 8000
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
