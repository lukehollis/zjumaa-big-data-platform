# 开发者文档 Documentation for Developers


多语言支持与本地化 i18n/l10n
---

### `.po`文件的生成与`.mo`文件的编译
- `python manage.py makemessages -l zh_Hans --extension html,txt,py,twig` # 生成`.po`文件于`zjumaa\locale`
- `python manage.py compilemessages` # 编译`.mo`文件
