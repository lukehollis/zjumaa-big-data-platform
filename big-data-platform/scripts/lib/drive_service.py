import googleapiclient
from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials
from httplib2 import Http


class DriveService(object):

    def __init__(self):
        self.FOLDER_ID = '1pkksUqbqlKu4zkG9WjCpiPFkt24eqZQe'

        # google api config
        scopes = ['https://www.googleapis.com/auth/drive']
        credentials = ServiceAccountCredentials.from_json_keyfile_name('credentials.json', scopes)
        # google api auth
        http_auth = credentials.authorize(Http())
        # init drive service
        self.drive_service = build('drive', 'v3', http=http_auth)

    def list_files(self):
        # list files
        request = self.drive_service.files().list().execute()
        files = request.get('files', [])
        for f in files:
            print(f)
        return files

    def download_file(self, filename):
        file_list = self.list_files()
        file_id = None
        for file in file_list:
            if file['name'] == filename:
                file_id = file['id']
        if file_id:
            # update
            local_fd = open(filename, 'wb')
            request = self.drive_service.files().get_media(fileId=file_id)
            media_request = googleapiclient.http.MediaIoBaseDownload(local_fd, request)
            while True:
                try:
                    download_progress, done = media_request.next_chunk()
                except Exception as error:
                    print('An error occurred: %s' % error)
                    return
                if download_progress:
                    print( 'Download Progress: %d%%' % int(download_progress.progress() * 100))
                if done:
                    print ('Download Complete')
                return
            local_fd.close()
        else:
            print("Archive export file not found. Please export archive first. ")

    def create_file(self, filepath):
        # upload file
        file_metadata = {
            'name': filepath,
            'parents': [self.FOLDER_ID],
            }
        media = googleapiclient.http.MediaFileUpload(f'{filepath}', 
            mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') # TODO: only xlsx for now
        file = self.drive_service.files().create(body=file_metadata,
                                            media_body=media,
                                            fields='id').execute()
        print(f"File Created - ID: {file.get('id')}") 
        return file.get('id')

    def update_file(self, filepath, file_id):
        # update file
        file_metadata = {
            'name': filepath,
            }
        media = googleapiclient.http.MediaFileUpload(f'{filepath}', 
            mimetype='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') # TODO: only xlsx for now
        file = self.drive_service.files().update(
            fileId=file_id,
            body=file_metadata,
            media_body=media).execute()
        print(f"File Updated - ID: {file.get('id')}") 

    def upsert_file(self, filename):
        # check if exist
        file_list = self.list_files()
        file_id = None
        for file in file_list:
            if file['name'] == filename:
                file_id = file['id']
        if file_id:
            # update
            self.update_file(filename, file_id)
        else:
            # create
            file_id = self.create_file(filename)
        file_edit_link = self.drive_service.files().get(fileId=file_id, fields='webViewLink').execute()
        return file_edit_link

    def delete_file(self, file_id):
        try:
            self.drive_service.files().delete(fileId=file_id).execute()
        except Exception as error:
            print(f"Delete failed - {error}")

    def purge_filename(self, filename):
        file_list = self.list_files()
        for file in file_list:
            if file['name'] == filename:
                print("Deleting file - file['name'] | file['id']")
                res = self.delete_file(file['id'])
