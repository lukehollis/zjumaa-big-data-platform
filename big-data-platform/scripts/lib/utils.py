import boto3
import shortuuid
from urllib.request import Request, urlopen
import urllib.parse
from slugify import slugify

from django.conf import settings

from app.models import Collection, File


def download_file_binary(URL):
    """download file content in binary from a URL
    
    Arguments:
        URL {[type]} -- [description]
    
    Returns:
        [type] -- [description]
    """
    req = Request(URL, headers={'User-Agent': 'Mozilla/5.0'})
    return urlopen(req).read()


def upload_file_data_to_s3(file_data, filename):
    """upload binary file data to s3 with input key
    
    Arguments:
        file_data {[type]} -- [description]
        key {[type]} -- [description]
    """
    aws_client = boto3.Session(
        aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
        aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY,
    )
    s3 = aws_client.resource('s3')
    key = f"{shortuuid.uuid()}-{filename}"
    upload_result = s3.Bucket(settings.AWS_STORAGE_BUCKET_NAME).put_object(
        Key=key,
        Body=file_data,
        ACL='public-read',
    )
    return upload_result


def save_remote_file(item, file_dict):
    s3uploaded = upload_file_data_to_s3(download_file_binary(file_dict['remote_url']), f"{slugify(str(file_dict['title']))}.jpg")
    print(f'File uploaded: {s3uploaded.key}')
    file = File(
        item=item,
        title=file_dict['title'],
        filename = s3uploaded.key,
        filepath = s3uploaded.key,
    )
    if 'mime_type' in file_dict:
        file.mime_type = file_dict['mime_type']
    file.save()
    return file


def upsert_collection(project, collection_name):
    collection = None
    if not Collection.objects.filter(project=project, title=collection_name).exists():
        collection = Collection(
            project=project,
            title=collection_name,
        )
        collection.save()
        print(f"Created Collection - {collection}")
    else:
        collection = Collection.objects.get(project=project, title=collection_name)
        print(f"Found Collection - {collection}")
    return collection
