"""[summary]
import an archive from xlsx

usage: python manage.py runscript import_archive --script-args my_archive
"""
import os, re

import pandas as pd

from app.models import Item

from .lib.drive_service import DriveService

from pprint import pprint


class Importer:

    def __init__(self, hostname):
        print(f'Importing hostname: {hostname}')
        self.hostname = hostname
        self.items = Item.objects.filter()
        self.local_filename = f'{self.hostname}.xlsx'

    def start(self):
        # download xlsx from drive

        # read xlsx as pandas dataframe
        df = pd.read_excel(self.local_filename)
        #pprint(df)

        for idx, row in df.iterrows():
            self._update_item(idx, row)
            break
            if idx > 1:
                break

    def _isNaN(self, num):
        return num != num

    def _get_meta_type(self, field_label):
        meta_type = 'TEXT'
        meta_type_in_label = field_label[field_label.find("(")+1:field_label.find(")")]
        if meta_type_in_label:
            meta_type = meta_type_in_label
        return meta_type

    def _update_item(self, idx, row):
        print()
        print(idx, row.keys())
        #print(row, row[0])
        # locate the item from db with ID
        item_id = row['ID']
        existing_item = self.project.items.get(pk=item_id)
        print(existing_item)

        # update item properties
        existing_item.title = row['Title']
        existing_item.description = row['Description']
        existing_item.privacy = row['Privacy']
        # save item model
        existing_item.save()


def run(*args):
    # init
    print(args)
    hostname = str(args[0])

    drive = DriveService()
    importer = Importer(hostname)

    importer.project.
    # download xlsx from drive
    drive.download_file(importer.local_filename)
    # import items from xlsx
    importer.start()

    print(f"Import completed. ")
