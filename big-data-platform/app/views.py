import asyncio
import subprocess
from dateutil.parser import parse

from django.conf import settings
from django import forms
from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse, resolve
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import PasswordChangeForm, AuthenticationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import Group
from django.core.paginator import Paginator
from django.contrib import messages
from django.db.models import IntegerField, Q, F
from django.db.models.functions import Cast
from django.contrib.auth.models import Group
from rest_framework import viewsets
from rest_framework import permissions

from .models import Collection, File, Item, CustomUser
from .serializers import UserSerializer, GroupSerializer, ItemSerializer

from .forms import SignUpForm

from pprint import pprint
#from time import time


def index(request):
    items = Item.objects.filter()
    collections = Collection.objects.filter()

    items_count = Item.objects.all().count()
    collections_count = Collection.objects.all().count()
    files_count = File.objects.all().count()

    return render(request, 'index.twig', {
        'items': items,
        'collections': collections,
        'items_count': items_count,
        'collections_count': collections_count,
        'files_count': files_count,
    })


def items(request):
    page_size = 24

    # init filter with project items
    items = Item.objects.filter()

    # filter
    for q in request.GET:
        if q in ['page']:
            continue

        label = q
        value = request.GET[q]

        if label == 'Tags':
            items = items.filter(files__file_tags__name=value)
        elif label == 'Color':
            items = items.filter(metadata__label='Color', metadata__value=value)
        # full text search in metadata values, item.title/desc
        elif label in ['s', 'keyword']:
            items = items.filter(Q(metadata__value__icontains=value) | Q(title__icontains=value) | Q(description__icontains=value)).distinct()
        else:
            items = items.filter(metadata__label=label, metadata__value=value)

    page = 1
    if request.GET.get('page'):
        page = int(request.GET.get('page'))
    pagination = Paginator(items, page_size)
    current_page = pagination.page(page)

    return render(request, 'items.twig', {
        'current_page': current_page,
        'pagination': pagination,
    })

def item(request, id, slug=None):
    item = Item.objects.get(id=id)

    files = File.objects.filter(item=item).all()
    first_file = files.first()
    collections = Collection.objects.filter(items=item)

    return render(request, 'item.twig', {
            'item': item,
            'files': list(files.values('id', 'title', 'mime_type', 'filename', 'filepath')),
            'first_file': first_file,
            'collections': collections
        })


def collections(request):
    page_size = 24

    # init filter with project items
    collections = Collection.objects.filter()

    # filter
    for q in request.GET:
        if q in ['page']:
            continue

        label = q
        value = request.GET[q]

        if label in ['s', 'keyword']:
            collections = collections.filter(Q(title__icontains=value) | Q(description__icontains=value)).distinct()

    page = 1
    if request.GET.get('page'):
        page = int(request.GET.get('page'))
    pagination = Paginator(collections, page_size)
    current_page = pagination.page(page)

    return render(request, 'collections.twig', {
        'current_page': current_page,
        'pagination': pagination,
    })

def collection(request, id, slug):
    collection = Collection.objects.get(id=id)
    items = collection.items.all()

    page_size = 24
    page = 1
    if request.GET.get('page'):
        page = int(request.GET.get('page'))
    pagination = Paginator(items, page_size)

    cover_image = None
    if collection.cover_image and collection.cover_image != "default.png":
        cover_image = collection.cover_image
    elif collection.items and collection.items.first() and collection.items.first().files:
        cover_image = collection.items.first().files.first()

    return render(request, 'collection.twig', {
        'collection': collection,
        'items': pagination.page(page),
        'cover_image': cover_image,
    })

def about_page(request):
    return render(request, 'page_about.twig')

def sign_up(request):
    registered = False

    # perform registration
    if request.method == 'POST':

        # TODO:
        form = SignUpForm(request.POST)

        if form.is_valid():
            # # handle captcha processing in the future when needed
            # # captcha = request.POST.get('g-recaptcha-response')
            #
            # custom_user = form.save()
            # custom_user.refresh_from_db()
            #
            # # load the profile instance created by the signal
            # custom_user.save()
            # raw_password = form.cleaned_data.get('password1')

            # login user after signing up

            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            password_repeat = form.cleaned_data.get('password2')

            custom_user = CustomUser.objects.create_user(username, username, password)
            custom_user.save()
            custom_user = authenticate(username=username, password=password)

            if custom_user:
                login(request, custom_user)
                redirect_to = request.GET.get('redirect_to', None)

                if redirect_to is not None and redirect_to != 'None':
                    return HttpResponseRedirect(redirect_to)
                return HttpResponseRedirect(reverse('user_profile'))
            else:
                messages.error(request, "There was an error creating your account.")
                return render(request, 'sign_up.twig', { 'form': form, 'messages': messages })
        else:
            messages.error(request, "There was an error creating your account.")
            return render(request, 'sign_up.twig', { 'form': form, 'messages': messages })

    form = SignUpForm()
    return render(request, 'sign_up.twig', { 'form': form, 'messages': messages })

def user_login(request):
    # perform login
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)

            if user:
                login(request, user)
                redirect_to = request.GET.get('redirect_to', None)

                # no idea why the failed get is being cast to a string
                if redirect_to is not None and redirect_to != 'None':
                    return HttpResponseRedirect(redirect_to)

                return HttpResponseRedirect(reverse('index'))

            else:
                return render(request, 'login.twig', { 'form': form, 'messages': messages })
        else:
            return render(request, 'login.twig', { 'form': form, 'messages': messages })

    # show login form
    else:
        form = AuthenticationForm()

        # render
        return render(request, 'login.twig', { 'form': form, 'messages': messages, 'redirect_to': request.GET.get('redirect_to') })

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect(reverse('index'))

@login_required
def user(request):
    return HttpResponseRedirect(reverse('user_saved'))

@login_required
def user_profile(request):

    # perform registration
    if request.method == 'POST':
        form = ProfileForm(request.POST, request.FILES)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            email = form.cleaned_data.get('email')
            full_name = form.cleaned_data.get('full_name')
            title = form.cleaned_data.get('title')
            affiliation = form.cleaned_data.get('affiliation')
            bio = form.cleaned_data.get('bio')
            picture = form.cleaned_data.get("picture")

            user = CustomUser.objects.get(username=request.user.username)
            user.username = username
            user.email = email
            user.full_name = full_name
            user.title = title
            user.affiliation = affiliation
            user.bio = bio
            if picture and len(request.FILES):
                user.picture = picture
            elif picture == False:
                user.picture = None
            user.save()

            user = CustomUser.objects.get(username=request.user.username)
            form = ProfileForm(initial={
                    'username': user.username,
                    'email': user.email,
                    'full_name': user.full_name,
                    'title': user.title,
                    'affiliation': user.affiliation,
                    'picture': user.picture,
                    'bio': user.bio,
                })

            messages.success(request, 'Your profile has been updated successfully.')
            return redirect('user_profile')

        else:
            messages.error(request, "There as an error processing your edits.")
            return render(request, 'user-profile.twig', { 'form': form, 'messages': messages })

    else:
        form = ProfileForm(initial={
                'username': request.user.username,
                'email': request.user.email,
                'full_name': request.user.full_name,
                'title': request.user.title,
                'affiliation': request.user.affiliation,
                'picture': request.user.picture,
                'bio': request.user.bio,
            })

    return render(request, 'user-profile.twig', { 'form': form })

@login_required
def user_change_password(request):

    # change pass
    if request.method == 'POST':
        form = PasswordChangeForm(user=request.user, data=request.POST)
        if form.is_valid():
            form.save()
            update_session_auth_hash(request, form.user)
            return HttpResponseRedirect(reverse('user_profile'))
        else:
            return render(request, 'user-change-password.twig', { 'form': form })

    else:
        form = PasswordChangeForm(user=request.user)

    return render(request, 'user-change-password.twig', { 'form': form })



class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = CustomUser.objects.all().order_by('-date_joined')
    serializer_class = UserSerializer
    permission_classes = [permissions.IsAuthenticated]

class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    permission_classes = [permissions.IsAuthenticated]

class ItemViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows items to be viewed or edited.
    """
    queryset = Item.objects.all()
    serializer_class = ItemSerializer

    def get_queryset(self):
        return Item.objects.filter().order_by('id')
