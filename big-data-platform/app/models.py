import urllib.request

from django.contrib.auth.models import AbstractUser
from django.db import models
from django_better_admin_arrayfield.models.fields import ArrayField
from django.utils.translation import gettext_lazy as _


PUBLIC = 'PUBLIC'
PRIVATE = 'PRIVATE'
PRIVACY_CHOICES = [
    (PUBLIC, 'Public'),
    (PRIVATE, 'Private'),
]
IMPORT_INIT = 'INIT'
IMPORT_DONE = 'DONE'
IMPORT_ERROR = 'ERROR'
IMPORT_PROGRESS = 'PROGRESS'
IMPORT_CHOICES = [
    (IMPORT_INIT, 'Not Exported'),
    (IMPORT_DONE, 'Ready to Import'),
    (IMPORT_ERROR, 'Import Error'),
    (IMPORT_PROGRESS, 'In Progress'),
]

class CustomUser(AbstractUser):
    full_name = models.CharField(
        max_length=256,
        blank=True,
        verbose_name=_('Full Name'))
    title = models.CharField(max_length=1024, blank=True)
    affiliation = models.CharField(max_length=1024, blank=True)
    bio = models.TextField(blank=True)
    tagline = models.TextField(blank=True)
    picture = models.ImageField(
        upload_to='profile_images', blank=True)

    def __str__(self):
        return self.username

class Collection(models.Model):
    title = models.CharField(max_length=256, null=True)
    description = models.TextField(blank=True, null=True)
    cover_image = models.FileField(default='default.png', max_length=256)
    items = models.ManyToManyField('Item', related_name='items', blank=True)

    def __str__(self):
        return f'{self.title}, {self.description}, {self.cover_image}'


class Item(models.Model):
    title = models.CharField(max_length=1024)
    name = models.CharField(max_length=1024, blank=True, null=True)
    former_name = models.CharField(max_length=1024, blank=True, null=True)
    general_registration_number = models.CharField(max_length=1024, blank=True, null=True)

    accession_date = models.DateTimeField(max_length=256, blank=True, null=True)
    museum_registration_number = models.CharField(max_length=1024, blank=True, null=True)
    date_of_admission = models.DateTimeField(max_length=256, blank=True, null=True)
    classified_number = models.CharField(max_length=1024, blank=True, null=True)
    storage_warehouse = models.CharField(max_length=1024, blank=True, null=True)
    entry_location = models.CharField(max_length=1024, blank=True, null=True)
    state = models.CharField(max_length=1024, blank=True, null=True)
    classification = models.CharField(max_length=1024, blank=True, null=True)
    texture = models.CharField(max_length=1024, blank=True, null=True)
    rank = models.CharField(max_length=1024, blank=True, null=True)
    age_type = models.CharField(max_length=1024, blank=True, null=True)
    era = models.CharField(max_length=1024, blank=True, null=True)
    traditional_quantity = models.IntegerField(blank=True, null=True, default=0)
    the_actual_amount = models.IntegerField(blank=True, null=True, default=0)
    volume = models.IntegerField(blank=True, null=True, default=0)
    quality = models.IntegerField(blank=True, null=True, default=0)
    size = models.IntegerField(blank=True, null=True, default=0)
    source_method = models.CharField(max_length=1024, blank=True, null=True)
    source_number = models.CharField(max_length=1024, blank=True, null=True)
    source_unit_or_individual = models.CharField(max_length=1024, blank=True, null=True)
    chronological_research_information = models.TextField(blank=True, null=True)
    area_type = models.CharField(max_length=1024, blank=True, null=True)
    area = models.CharField(max_length=1024, blank=True, null=True)
    humanistic_type = models.CharField(max_length=1024, blank=True, null=True)
    humanities = models.CharField(max_length=1024, blank=True, null=True)
    function_category = models.CharField(max_length=1024, blank=True, null=True)
    character_biography = models.CharField(max_length=1024, blank=True, null=True)
    morphological_characteristics = models.CharField(max_length=1024, blank=True, null=True)
    craftsmanship = models.CharField(max_length=1024, blank=True, null=True)
    degree_of_completeness = models.CharField(max_length=1024, blank=True, null=True)
    unique_mark = models.CharField(max_length=1024, blank=True, null=True)
    incomplete_condition = models.CharField(max_length=1024, blank=True, null=True)
    colour = models.CharField(max_length=1024, blank=True, null=True)
    luster = models.CharField(max_length=1024, blank=True, null=True)
    type_of_text = models.CharField(max_length=1024, blank=True, null=True)
    font_category = models.CharField(max_length=1024, blank=True, null=True)
    handwriting_color = models.CharField(max_length=1024, blank=True, null=True)
    search_through = models.CharField(max_length=1024, blank=True, null=True)
    unearthed = models.CharField(max_length=1024, blank=True, null=True)
    the_current_situation = models.TextField(blank=True, null=True)
    storage_conditions = models.TextField(blank=True, null=True)
    cause_of_damage = models.TextField(blank=True, null=True)
    protection_priority = models.CharField(max_length=1024, blank=True, null=True)
    protective_measures_to_be_taken = models.TextField(blank=True, null=True)

    description = models.TextField(blank=True, null=True)
    attachment_situation = models.TextField(blank=True, null=True)

    name_title = models.CharField(max_length=1024, blank=True, null=True)
    author = models.CharField(max_length=1024, blank=True, null=True)
    source = models.CharField(max_length=1024, blank=True, null=True)
    date = models.DateTimeField(max_length=256, blank=True, null=True)

    def __str__(self):
        return self.title

    def to_dict(self):
        res = {
            'ID': self.id,
            'Title': self.title,
            'Description': self.description,
            'Files': '| '.join([file.filepath.url for file in self.files.all()]),
        }

        return res


class File(models.Model):
    item = models.ForeignKey(Item, related_name='files', on_delete=models.CASCADE)
    title = models.CharField(max_length=1024, blank=True, null=True)
    mime_type = models.CharField(max_length=255, blank=True, null=True)
    filename = models.CharField(max_length=1024, blank=True, null=True)
    filepath = models.FileField(max_length=256)

    def save(self, *args, **kwargs):
        super(File, self).save(*args, **kwargs)
        if not self.title:
            self.title = self.filepath.name
        self.filename = self.filepath.name
        if not self.mime_type:
            with urllib.request.urlopen(self.filepath.url) as response:
                info = response.info()
                self.mime_type=info.get_content_type()
        super(File, self).save(*args, **kwargs)
        try:
            self._get_ai_results()
        except Exception as e:
            print(e)
            # todo: log this somewhere
            pass

    def __str__(self):
        return self.filename

class FileTag(models.Model):
    name = models.CharField(max_length=1024)
    generated_by = models.CharField(max_length=256, blank=True, null=True)
    file = models.ForeignKey(File, related_name='file_tags', on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        unique_together = ('file', 'name',)
