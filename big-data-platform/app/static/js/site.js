// get a specific variable from query string
const getQueryVariable = (variable) => {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    if (pair[0] == variable) {
      return pair[1];
    }
  }
  return false;
}

// parse querystring to object
const parseQuery = (queryString) => {
  var query = {};
  var pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
  for (var i = 0; i < pairs.length; i++) {
    var pair = pairs[i].split('=');
    if (decodeURIComponent(pair[0]).length) {
      query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
    }
  }
  return query;
}

// serialize object to query string
const serializeQuery = (obj) => {
  var str = [];
  for (var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
}

// video sync
function VideoSync(_at_primary, _at_secondary) {
  this.primary = _at_primary;
  this.secondary = _at_secondary;
  this.ready = false;
  this.secondary.addEventListener('canplay', (function(_this) {
    return function() {
      return _this.ready = true;
    };
  })(this));
  this.primary.addEventListener('play', (function(_this) {
    return function() {
      return _this.secondary.play();
    };
  })(this));
  this.primary.addEventListener('pause', (function(_this) {
    return function() {
      return _this.secondary.pause();
    };
  })(this));
  this.primary.addEventListener('timeupdate', this.sync);
  this.primary.addEventListener('seeking', this.sync);

  this.secondary.addEventListener('play', (function(_this) {
    return function() {
      return _this.primary.play();
    };
  })(this));
  this.secondary.addEventListener('pause', (function(_this) {
    return function() {
      return _this.primary.pause();
    };
  })(this));
  this.secondary.addEventListener('timeupdate', this.sync_secondary);
  this.secondary.addEventListener('seeking', this.sync_secondary);
}

VideoSync.prototype.sync = function() {
  if (this.ready) {
    return this.secondary.currentTime = this.primary.currentTime;
  }
};

VideoSync.prototype.sync_secondary = function() {
  return this.primary.currentTime = this.secondary.currentTime;
};

const setupPage = () => {
  // hide commentary section of sign up form on default
  $("#commentary_form").hide();
  $(document).on('click', '#create_archive', function() {
    if (this.checked) {
      $("#commentary_form").show();
    } else {
      $("#commentary_form").hide();
    }
  });

  $('.toggleDescriptionButton').on('click', function(e) {
    $('.shortDescription').hide();
    $('.longDescription').show();
  });

  $('.toggleDescriptionButtonLong').on('click', function(e) {
    $('.shortDescription').show();
    $('.longDescription').hide();
    $(document).scrollTop($(".archive-info").offset().top);
  });


  $('.headerExternalButtonSearch').on('click', function(e) {
    $('header').addClass('-showSearch');
    $('.headerExternalSearch').removeClass('-hidden');
    $('#header_external_search').focus();
  });

  $('#header_external_search_back').on('click', function(e) {
    $('header').removeClass('-showSearch');
    $('.headerExternalSearch').addClass('-hidden');
    if (window.location.pathname === '/items/') {
      window.history.back();
    }
  });

  if (window.location.pathname === '/items/') {
    $('header').addClass('-onSearchRoute');
    $('.headerExternalSearch').removeClass('-hidden');
  }

  $('.headerExternalSearchBackground').on('click', function(e) {
    $('header').removeClass('-showSearch');
    $('.headerExternalSearch').addClass('-hidden');
  });

  $(document).keyup(function(e) {
    if (e.key === "Escape") {
      $('header').removeClass('-showSearch');
      $('.headerExternalSearch').addClass('-hidden');
    }
  });

  $('#search_form').submit(function(e) {
    e.preventDefault();
    window.location.replace(`/items/?s=${$('#header_external_search').val()}`);
  });

  var textSearchVal = getQueryVariable('s');
  if (textSearchVal && textSearchVal.length) {
    $('#header_external_search').val(textSearchVal);
  }


  $('.helpButton').on("click", () => {
    window.$crisp = [];
    window.CRISP_WEBSITE_ID = "40cffd76-8164-4541-b8b1-5b226c11e855";
    (function() {
      d = document;
      s = d.createElement("script");
      s.src = "https://client.crisp.chat/l.js";
      s.async = 1;
      d.getElementsByTagName("head")[0].appendChild(s);
    })();
    $crisp.push(['do', 'chat:open']);
  });

  $('.filterPanelSummary').on('click', (e) => {
    let $filter = $(e.target).parents('.filter');
    $filter.toggleClass("-facetsShown");
  });

  $('.facet').on('click', (e) => {
    let $facet = $(e.target);
    if (!$facet.hasClass('facet')) {
      $facet = $facet.parents('.facet');
    }
    $facet.toggleClass("-toggled");

    const filter = $facet.data('filter');
    const value = $facet.data('value');
    const query = parseQuery(window.location.search);
    let removeFilter = false;

    if (filter in query) {
      const values = query[filter].split(',');
      if (values.indexOf(value) >= 0) {
        values.splice(values.indexOf(value), 1);
        if (!values.length) {
          removeFilter = true;
        }
      } else {
        values.push(value);
      }

      if (removeFilter) {
        delete query[filter];
      } else {
        query[filter] = values;
      }

    } else {
      query[filter] = value;
    }

    query.page = 1;
    window.location = `${window.location.href.replace(window.location.search, '')}?${serializeQuery(query)}`;
  });

  // close audio player
  $('.closeAvPlayer').on('click', () => {
    $('.avPlayerAudio').addClass('-avPlayerAudioHidden');
    $('.avPlayerVideo').addClass('-avPlayerVideoHidden');

    const $audioPlayer = $('.avPlayerAudio audio');
    const $videoPlayer = $('.avPlayerVideo video');
    $audioPlayer[0].pause();
    $videoPlayer[0].pause();

    // set player state as dismissed
    $("#av_player").addClass('-dismissed');
  });

  // set video content in persistent media player
  $(document).on('click', 'video', (e) => {
    let $target = $(e.target);
    const src = $target.attr('src');
    window.__zjumaa__ = {} || window.__zjumaa__;
    const Or = window.__zjumaa__;
    Or.itemPlayingMedia = window.location.pathname;
    // set new player state as not dismissed
    $("#av_player").removeClass('-dismissed');

    // sync video
    const $avPlayer = $('.avPlayerVideo video');
    if (src !== $avPlayer.attr('src')) {
      $avPlayer.attr('src', src);
      $avPlayer[0].load();
      new VideoSync($('.MediaViewer video')[0], $('.avPlayerVideo video')[0]);
      $('.avPlayerVideo video').prop('muted', true);
    }

    // show/hide video player when not on route anymore or scrolled out of view
    $(document).scroll(() => {
      // ensure element is not dismissed (otherwise if user has dismissed player, player will return on scroll)
      // TODO: in the future determine how best to unbind this specific scroll event without unbinding others
      if (!$("#av_player").hasClass('-dismissed')) {
        $target = $(e.target);

        if ($target.length && window.location.pathname === window.__zjumaa__.itemPlayingMedia) {
          if (window.pageYOffset > $target.offset().top + $target.outerHeight()) {
            $('.avPlayerVideo').removeClass('-avPlayerVideoHidden');
          } else {
            $('.avPlayerVideo').addClass('-avPlayerVideoHidden');
          }
        } else {
          $('.avPlayerVideo').removeClass('-avPlayerVideoHidden');
        }
      }
    });
  });

  // set audio content in persistent media player
  $(document).on('click', '#audio_button', (e) => {
    let $target = $(e.target);
    const audioFileUrl = $target.data('audiofileurl');
    window.__zjumaa__ = {} || window.__zjumaa__;
    const Or = window.__zjumaa__;
    Or.itemPlayingMedia = window.location.pathname;
    // set new player state as not dismissed
    $("#av_player").removeClass('-dismissed');

    // sync video
    const $avPlayer = $('.avPlayerAudio audio');
    if (audioFileUrl !== $avPlayer.attr('src')) {
      $avPlayer.attr('src', audioFileUrl);
      $avPlayer[0].load();
      $avPlayer[0].play();
    }

    $('.avPlayerAudio').removeClass('-avPlayerAudioHidden');
  });



  const ESC = 27;
  $(document).on('keydown', (e) => {
    if (e.which === ESC) {
      $(".modal").addClass("-hidden");
    }
  });

  $(document).on('click', '.modalClose', (e) => {
    $(".modal").addClass("-hidden");
  });

  $(document).on('click', '.horizontalListOpenableViewAllButton', (e) => {
    e.preventDefault();
    $('.horizontalList').toggleClass('-opened')
  });
}

/**
(function($) {
  $(document).ready(function() {
    setupPage();
  });
  document.addEventListener('turbolinks:load', function(){ setupPage() });
})(jQuery);
*/


/**
// turbolinks hack for continuing state of audio and video elements across page load
;
(function() {
  var forEach = Array.prototype.forEach
  var selector = [
    '[data-turbolinks-permanent] audio',
    'audio[data-turbolinks-permanent]',
    '[data-turbolinks-permanent] video',
    'video[data-turbolinks-permanent]'
  ].join(',')

  document.addEventListener('turbolinks:load', function() {
    var mediaElements = document.querySelectorAll(selector)
    forEach.call(mediaElements, uninterrupt)
  })

  // Bind to turbolinks:render to handle previews
  document.addEventListener('turbolinks:render', function() {
    var mediaElements = document.querySelectorAll(selector)
    forEach.call(mediaElements, function(media) {
      if (media.isPlaying) {
        // settimeout necessary, investigate why in the future
        setTimeout(() => {
          media.play();
        }, 300);
      }
    })
  })

  function uninterrupt(media) {
    if (!media.uninterrupted) {
      media.addEventListener('play', function() {
        media.isPlaying = true
      })
      media.addEventListener('pause', function() {
        media.isPlaying = false
      })
      media.uninterrupted = true;
    }
  }
})()
*/
