from django.conf import settings

def iiif_settings(request):
    return {
        'IIIF_SERVER': settings.IIIF_SERVER,
        'AWS_STORAGE_BUCKET_NAME': settings.AWS_STORAGE_BUCKET_NAME,
    }
