import json

from django import template

from app.models import Item

register = template.Library()

@register.filter(name='related_items')
def related_items(value):

    try:
        # id_list = [meta['id'] for meta in value_json]
        id_list = value['ids']
    except:
        id_list = []
    return Item.objects.filter(id__in=id_list).all()
