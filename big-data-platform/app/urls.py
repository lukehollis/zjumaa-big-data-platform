"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from rest_framework import routers

from . import views
from .admin import dashboard


# register api routes with api router
router = routers.DefaultRouter()
router.register(r'items', views.ItemViewSet)


urlpatterns = [
    path('', views.index, name="index"),

    # items
    path('items/<str:id>/<str:slug>', views.item, name="item"),
    path('items/<str:id>/', views.item, name="item"),
    path('items/', views.items, name="items_index"),
    path('search/', views.items, name="items_index"),

    # collections
    path('collections/<str:id>/<str:slug>', views.collection, name="collection"),
    path('collections/<str:id>/', views.collection, name="collection"),
    path('collections/', views.collections, name="collections"),

    # auth
    path('accounts/', include('django.contrib.auth.urls')),
    path('profile/', views.user_profile, name='profile'),
    path('sign-up/', views.sign_up, name='sign_up'),
    path('login/', views.user_login, name='login'),
    path('logout/', views.user_logout, name='logout'),

    # api
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
