# for a given query, set which filters are currently active in the UI
def set_active_filters(filters, query):
    for filter in filters:
        for value in filter['values']:
            for param in query.lists():
                selected_values = []
                if len(param[1]):
                    selected_values = param[1][0].split(",")
                if filter['label'] == param[0] and str(value['value']) in selected_values:
                    filter['active'] = True
                    value['active'] = True

    return filters
