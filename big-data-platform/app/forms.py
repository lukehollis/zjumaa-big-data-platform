from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm

from .models import CustomUser

class CustomUserCreationForm(UserCreationForm):
    class Meta:
        model = CustomUser
        fields = ('username', 'email', 'full_name', 'title', 'affiliation')

class SignUpForm(UserCreationForm):
    class Meta:
        model = CustomUser
        fields = ('username', 'password1', 'password2')

class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = ('username', 'email', 'full_name', 'title', 'affiliation', 'bio', 'picture')

class ProfileForm(forms.Form):
    username = forms.CharField(max_length=256)
    email = forms.CharField(max_length=256)
    full_name = forms.CharField(max_length=256)
    title = forms.CharField(max_length=1024, required=False)
    affiliation = forms.CharField(max_length=1024, required=False)
    picture = forms.ImageField(required=False)
    bio = forms.CharField(widget=forms.Textarea, required=False)

    class Meta:
        model = CustomUser
        fields = ['username', 'email', 'full_name', 'title', 'affiliation', 'picture', 'bio']
