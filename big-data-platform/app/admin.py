from django.contrib import admin
from django.contrib.admin.sites import AdminSite
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.db.models import Q
from django.forms import BaseInlineFormSet, ModelForm
from django.utils.safestring import mark_safe
from django.urls import reverse
from django_better_admin_arrayfield.admin.mixins import DynamicArrayMixin
from django.utils.translation import gettext_lazy as _

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser, Collection, Item, File, FileTag

from pprint import pprint


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ['email', 'username', 'full_name']
    fieldsets = UserAdmin.fieldsets + (
            (None, {'fields': ('full_name', 'bio', 'tagline', 'picture')}),
    )


# dashboard admin site
class Dashboard(AdminSite):
    pass

# initiate dashboard admin panel and customize labels
dashboard = Dashboard(name='dashboard')
dashboard.site_header = _('Archive Dashboard')
dashboard.site_title = _('Archive Dashboard')
dashboard.index_title = ''


class UserProfile(CustomUser):
    class Meta:
        proxy = True
        verbose_name = 'User Profile'
        verbose_name_plural = 'User Profile'

class UserProfileAdmin(admin.ModelAdmin):
    readonly_fields = ('username',)
    exclude = ('password', 'is_superuser', 'is_staff', 'is_active', 'last_login', 'date_joined', 'groups', 'user_permissions', 'full_name' )
    # disable add action
    def has_add_permission(self, request, obj=None):
        return False
    def get_queryset(self, request):
        qs = super(UserProfileAdmin, self).get_queryset(request)
        return qs.filter(id=request.user.id)

class EditLinkToInlineObject(object):
    def edit_link(self, instance):
        url = reverse('dashboard:%s_%s_change' % (
            instance._meta.app_label,  instance._meta.model_name),  args=[instance.pk] )
        if instance.pk:
            return mark_safe(u'<a href="{u}">edit</a>'.format(u=url))
        else:
            return ''

class FileTagInline(admin.StackedInline):
    model = FileTag
    extra = 0

class FileInline(EditLinkToInlineObject, admin.StackedInline):
    model = File
    extra = 0

class FileAdmin(admin.ModelAdmin):
    inlines = [
        FileTagInline,
    ]

    list_display = ['title', 'mime_type', 'filename', 'item', ]
    search_fields = ('filename', 'item__title', )

    def get_queryset(self, request):
        qs = super(FileAdmin, self).get_queryset(request)
        return qs.filter()

class CollectionInline(admin.TabularInline):
    model = Collection.items.through
    extra = 0

class ItemAdmin(admin.ModelAdmin):
    list_display = ['title', ]

    search_fields = ('title', 'description', 'items__title')

    inlines = [
        FileInline,
        CollectionInline,
    ]

    def get_queryset(self, request):
        qs = super(ItemAdmin, self).get_queryset(request)
        return qs.filter()

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    def save_model(self, request, obj, form, change):
        obj.save()

class CollectionAdmin(admin.ModelAdmin):
    exclude = ('items',)

    def get_queryset(self, request):
        qs = super(CollectionAdmin, self).get_queryset(request)
        return qs.filter()
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


# Add models to the admin panel
admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Collection)
admin.site.register(File)
admin.site.register(FileTag)
admin.site.register(Item, ItemAdmin)

# add customized admin models to dashboard
dashboard.register(UserProfile, UserProfileAdmin)
dashboard.register(Item, ItemAdmin)
dashboard.register(Collection, CollectionAdmin)
dashboard.register(File, FileAdmin)
