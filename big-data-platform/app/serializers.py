from django.contrib.auth.models import Group
from .models import CustomUser, Item, File
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']

class FileSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = File
        fields = ['title', 'mime_type', 'filename', 'filepath']

class ItemSerializer(serializers.HyperlinkedModelSerializer):
    files = FileSerializer(many=True, read_only=True)

    class Meta:
        model = Item
        fields = ['id', 'title', 'description', 'privacy', 'files']
