# environment dependent settings
import os

# this enables pre-built static files to be located on production server
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
STATIC_ROOT=os.environ['STATIC_ROOT'] if 'STATIC_ROOT' in os.environ else os.path.join(BASE_DIR, "staticfiles")

# database config
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ['DATABASE_NAME'] if 'DATABASE_NAME' in os.environ else 'NAME_OF_YOUR_DB',
        'USER': os.environ['DATABASE_USER'] if 'DATABASE_USER' in os.environ else 'USER_OF_YOUR_DB',
        'PASSWORD': os.environ['DATABASE_PASSWORD'] if 'DATABASE_PASSWORD' in os.environ else 'PASSWORD_OF_YOUR_DB',
        'HOST': os.environ['DATABASE_HOST'] if 'DATABASE_HOST' in os.environ else '127.0.0.1',
    }
}

# email config
SENDGRID_API_KEY = os.environ['SENDGRID_API_KEY'] if 'SENDGRID_API_KEY' in os.environ else os.getenv('SENDGRID_API_KEY')
EMAIL_HOST = 'smtp.sendgrid.net'
EMAIL_HOST_USER = os.environ['EMAIL_HOST_USER'] if 'EMAIL_HOST_USER' in os.environ else 'apikey'
EMAIL_HOST_PASSWORD = SENDGRID_API_KEY
EMAIL_PORT = 587
EMAIL_USE_TLS = True

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ['SECRET_KEY'] if 'SECRET_KEY' in os.environ else 'YOUR_SECRET',

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.environ['DEBUG'] if 'DEBUG' in os.environ else False,

# media storage to s3
AWS_ACCESS_KEY_ID = os.getenv('AWS_ACCESS_KEY_ID', 'key')
AWS_SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY', 'sec')
AWS_STORAGE_BUCKET_NAME = os.getenv('AWS_STORAGE_BUCKET_NAME', 'bucket')
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
AWS_S3_OBJECT_PARAMETERS = {
    'CacheControl': 'max-age=86400',
}
AWS_LOCATION = ''
AWS_S3_FILE_OVERWRITE = False

AWS_MEDIA_URL = F'https://bucket.s3.amazonaws.com/{AWS_LOCATION}/'

# subdomain cookies
# SESSION_COOKIE_DOMAIN = os.getenv('SESSION_COOKIE_DOMAIN', '.orphe.us')
# DOMAIN_NAME = os.getenv('DOMAIN_NAME', 'orphe.us')
#
# IIIF server
IIIF_SERVER = "https://zju.edu.cn"

# data-grinder-api
DATA_GRINDER_API = "http://api.data-grinder.archimedes.digital"
